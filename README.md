## Crossword Style Game ##
*Only Html and CSS were used to create this game.*
Inspired by student project. I attempt to make my own version of a games they have submitted using only HTML and CSS.

This is a crossword, with clues listed below the board. You have the option to turn on validation, which once turned on cannot be turned off. Clues are crossed off once all the letters have been entered, if validation is turned on then the clues are only crossed off when the correct answer has been entered.

---

## Extra files used ##
List of files besides out index.html and style.css

1. Google Fonts (Courgette, Inconsolata, Raleway) to make it look a little nicer
2. Generated the board using https://www.puzzle-maker.com/crossword_Design.cgi
3. PHP script and Excel file used to help create the HTML for the game board instead of manually writing out every singel cell. (includes the EXCEL files used to plan layout of boards)

---

## JavaScript ##
If I had include JavScript, what would be different?

1. Easier to check for correct answers (maybe)
2. Possibility of tabbing to work in the direction you are solving (maybe)
3. Highlighting clues could have been done easier (especially if we could anticipate the direction you are solving)
4. PHP script and EXCEL to track the clues for each cells